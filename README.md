# Xplode
A script to reset your Xcodes

Currently compatible with Xcode 10 and Xcode 11

Running Xplode will:
* Clean Build Folder
* Quit Xcode
* Delete Derived Data
* Reset all simulators
* Reopen Xcode

## Installation

From Homebrew:

1. `brew tap mplorentz/tap https://gitlab.com/mplorentz/tap`
2. `brew install xplode`

From source:

1. `git clone git@gitlab.com:mplorentz/xplode.git`
2. `cd xplode && chmod u+x xplode`
3. Add the xplode script to your `PATH`

## Usage

Open Terminal and type `xplode`

## Notes
* Xplode may not quit or restart Xcode properly if you have multiple versions of it open.
* Xplode will `rm -rf` your entire DerivedData folder which holds the derived data for all your Xcode projects. 
* Xplode is a bundle of powerful and dangerous exploding magic. Use at your own risk.

## Contributing

1. Open an issue
2. Fork the repo
3. Create a feature branch for your changes (`git checkout -b my-new-feature`)
4. Commit your changes to your feature branch (`git commit -am 'Add some feature'`)
5. Push the branch (`git push origin my-new-feature`)
6. Create new Merge Request to merge your feature branch into Xplode/master
